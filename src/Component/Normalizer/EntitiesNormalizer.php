<?php
namespace SM\SilexRestApi\Component\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class EntitiesNormalizer implements NormalizerInterface
{
	private function normalizeEntity($object){
		$data = array();

		$reflectionClass = new \ReflectionClass($object);
		
		if($reflectionClass->hasMethod('toArray')){
			return $object->toArray();
		}
		
		foreach ($reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC) as $reflectionMethod) {
			if (strtolower(substr($reflectionMethod->getName(), 0, 3)) !== 'get') {
				continue;
			}

			if ($reflectionMethod->getNumberOfRequiredParameters() > 0) {
				continue;
			}

			$property = lcfirst(substr($reflectionMethod->getName(), 3));
			$value = $reflectionMethod->invoke($object);
			if(is_scalar($value)){
				$data[$property] = $value;
			}
		}
		return $data;
	}

	public function normalize($object, $format = NULL, array $context = array()){
		if(is_scalar($object)){
			return $object;
		}
		if(is_array($object) ){
			if(array_key_exists(0,$object) && array_key_exists(count($object)-1,$object)){
				return array_map(function($v){
					return $this->normalize($v);
				},$object);
			}else{
				return $object;
			}
		}else{
			return $this->normalizeEntity($object);
		}
	}

	public function supportsNormalization($data, $format = null){
		return true;
	}

}