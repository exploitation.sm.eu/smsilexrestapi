<?php
namespace SM\SilexRestApi\Component;

class Utilities{
	/**
	 * array_map with key and values :$callback($k,$v)
	 */
	public static function array_map_keys($callback,Array $aArray=array()){
		if(!is_callable($callback)){
			throw new \InvalidArgumentException('$callback not callable');
		}
		$aResult = array();
		foreach($aArray as $k=>$v){
			$aItem = $callback($k,$v);
			$aResult[$aItem[0]]=$aItem[1];
		}
		return $aResult;
	}

	public static function akead($k,$a,$d){
		return (is_array($a) && array_key_exists($k,$a))?$a[$k]:$d;
	}
}