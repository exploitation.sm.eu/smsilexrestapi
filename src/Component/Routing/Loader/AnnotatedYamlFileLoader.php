<?php
namespace SM\SilexRestApi\Component\Routing\Loader;

use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\Yaml\Parser as YamlParser;
use Silex\Application;
use Symfony\Component\Config\FileLocatorInterface;


class AnnotatedYamlFileLoader extends YamlFileLoader{
	private static $availableKeys = array(
		'resource', 'type', 'prefix', 'pattern', 'path', 'host', 'schemes', 'methods', 'defaults', 'requirements', 'options', 'condition','controllerProvider'
	);
	private $yamlParser;
	private $app;

	/**
	 * Constructor.
	 *
	 * @param FileLocatorInterface $locator A FileLocatorInterface instance
	 */
	public function __construct(FileLocatorInterface $locator,Application $app)
	{
		$this->locator = $locator;
		$this->app = $app;
	}

	/**
	 * Parses an import and adds the routes in the resource to the RouteCollection.
	 *
	 * @param RouteCollection $collection A RouteCollection instance
	 * @param array           $config     Route definition
	 * @param string          $path       Full path of the YAML file being processed
	 * @param string          $file       Loaded file name
	 */
	protected function parseImport(RouteCollection $collection, array $config, $path, $file)
	{
		$type = isset($config['type']) ? $config['type'] : null;
		$prefix = isset($config['prefix']) ? $config['prefix'] : '';

		if($type=='annotation' && substr($config['resource'],0,1)=="@"){
	        $defaults = isset($config['defaults']) ? $config['defaults'] : array();
	        $requirements = isset($config['requirements']) ? $config['requirements'] : array();
	        $options = isset($config['options']) ? $config['options'] : array();
	        $host = isset($config['host']) ? $config['host'] : null;
	        $condition = isset($config['condition']) ? $config['condition'] : null;
	        $schemes = isset($config['schemes']) ? $config['schemes'] : null;
	        $methods = isset($config['methods']) ? $config['methods'] : null;

	        $this->setCurrentDir(dirname($path));

	        $className = substr($config['resource'],1);
	        $routeCollection = $this->app['annot']->process($className, false, true);

	        /* @var $subCollection RouteCollection */
	        $subCollection = $routeCollection->flush($prefix);
	        
	        if (null !== $host) {
	            $subCollection->setHost($host);
	        }
	        if (null !== $condition) {
	            $subCollection->setCondition($condition);
	        }
	        if (null !== $schemes) {
	            $subCollection->setSchemes($schemes);
	        }
	        if (null !== $methods) {
	            $subCollection->setMethods($methods);
	        }
	        $subCollection->addDefaults($defaults);
	        $subCollection->addRequirements($requirements);
	        $subCollection->addOptions($options);

			$collection->addCollection($subCollection);
			//$this->app->mount($prefix,new $className());
			return;
		}
		parent::parseImport($collection, $config, $path, $file);
	}
}