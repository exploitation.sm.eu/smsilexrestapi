<?php
namespace  SM\SilexRestApi\Provider\Security\JWToken;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\HttpFoundation\Request;
use  SM\SilexRestApi\Provider\Security\JWToken\JWTokenUserToken;
use Firebase\JWT\JWT;

class JWTokenListener implements ListenerInterface
{
	const urlKey = 'X-Access-Token';
	const headerKey = 'X-Access-Token';
	const tokenPrefix = 'Bearer';

	var $jwtKey = "Very_secret_key";

	protected $tokenStorage;

	public function __construct(TokenStorageInterface $tokenStorage){
		$this->tokenStorage = $tokenStorage;
	}

	public function handle(GetResponseEvent $event){
		$request = $event->getRequest();
		//firewall is disabled during preflight
		if($request->getMethod() === "OPTIONS" && $request->headers->has("Access-Control-Request-Method")){
			$token = new JWTokenUserToken([]);
			$token->setUser('-');
			$this->tokenStorage->setToken($token);
			return;
		}
		try{
			$oJwtToken = $this->getJWTTokenFromRequest($request);
			$token = new JWTokenUserToken($oJwtToken->roles);
			$token->setUser($oJwtToken->name);
			$this->tokenStorage->setToken($token);
			return;
		} catch (AuthenticationException $failed) {
			// By default deny authorization
			//die('----'.$failed->getMessage());
			$response = new Response();
			$response->setStatusCode(Response::HTTP_FORBIDDEN);
			$response->setContent($failed->getMessage());
			$event->setResponse($response);
			// ... you might log something here

			// To deny the authentication clear the token. This will redirect to the login page.
			// Make sure to only clear your token, not those of other authentication listeners.
			// $token = $this->tokenStorage->getToken();
			// if ($token instanceof JWTokenUserToken && $this->providerKey === $token->getProviderKey()) {
			//     $this->tokenStorage->setToken(null);
			// }
			// return;
		}

	}

	protected function getJWTTokenFromRequest(Request $request) {
		$request_token = $this->extractFromRequest($request);
		if(empty($request_token)) {
			throw new AuthenticationException('Token not found or empty');
			//return null;
		}
		try {
			return JWT::decode($request_token, $this->jwtKey, array('HS256'));
		}catch(\Exception $ex) {
			throw new AuthenticationException('Decode error '.$ex->getMessage().' '.$request_token);
			//return null;
		}
	}

	protected function extractFromRequest(Request $request) {
		$sTmp = $request->headers->get(self::headerKey, $request->get(self::urlKey));
		return str_replace(self::tokenPrefix,'',$sTmp);
	}

	/*protected function getNewTokenForUser(UserInterface $user,array $options=array()){
		return JWT::encode(array(
			'name'	=> $user->getUsername(),
			'roles'	=> $user->getRoles(),
			'exp'	=> time() + 24*60*60//$this->lifeTime
		), $options['key']);
	}*/

}