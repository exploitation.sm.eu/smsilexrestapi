<?php
namespace  SM\SilexRestApi\Provider\Security\JWToken;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class JWTokenControllerProvider implements ControllerProviderInterface
{
	const VALIDATE_CREDENTIALS = '/validate';
	protected $baseRoute;

	public function setBaseRoute($baseRoute)
	{
		$this->baseRoute = $baseRoute;
		return $this;
	}

	public function connect(Application $app)
	{
		$controllers = $app['controllers_factory'];
		$controllers->post ( self::VALIDATE_CREDENTIALS, function (Request $request) use($app) {
			$token = new UsernamePasswordToken($request->get('username'), $request->get('password'), '-');
			$JWTokenProvider = new JWTokenProvider($app['security.users'],$app);
			$user = $JWTokenProvider->authenticate( $token );
			return $app->json ( [
				'status'	=> !!$user,
				'info'		=> !!$user ? [
					'token'		=> $JWTokenProvider->getUserToken( $user->getUser()),
					'user'		=> $user->getUsername(),
					'roles'		=> implode(',',$user->getUser()->getRoles()),
				] : [ ]
			] );
		} );
		return $controllers;
	}
}