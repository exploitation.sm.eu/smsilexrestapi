<?php

namespace  SM\SilexRestApi\Provider\Security\JWToken;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SM\SilexRestApi\Security\User\UserProvider;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class JWTokenServiceProvider implements ServiceProviderInterface {
	protected $app;

	public function register(Application $app) {
		$this->app = $app;

		$app['security.authentication_listener.factory.jwtoken'] = $app->protect(function ($name, $options) use ($app) {
			$app['security.authentication_provider.' . $name . '.jwtoken'] = function() use ($app,$name){
				return new JWTokenProvider($app['security.users'],$app);
			};

			$app['security.authentication_listener.'.$name.'.jwtoken'] = function() use ($app){
				return new JWTokenListener($app['security']);
			};

			return array(
				'security.authentication_provider.'.$name.'.jwtoken',
				'security.authentication_listener.'.$name.'.jwtoken',
				null,
				'pre_auth'
			);
		});
	}

	public function boot(Application $app) {
	}
}