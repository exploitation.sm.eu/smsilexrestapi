<?php
namespace  SM\SilexRestApi\Provider\Security\JWToken;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class JWTokenUserToken extends AbstractToken
{
	public function __construct(array $roles = array())
	{
		parent::__construct($roles);

		// If the user has roles, consider it authenticated
		$this->setAuthenticated(count($roles) > 0);
	}

	public function getCredentials()
	{
		return '';
	}
}