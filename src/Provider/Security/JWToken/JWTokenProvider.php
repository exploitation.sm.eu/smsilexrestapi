<?php
namespace  SM\SilexRestApi\Provider\Security\JWToken;
// http://symfony.com/doc/current/cookbook/security/custom_authentication_provider.html

use Firebase\JWT\JWT;
use SM\SilexRestApi\Provider\Security\JWToken\JWTokenUserToken;
use SM\SilexRestApi\Silex\Application;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class JWTokenProvider implements AuthenticationProviderInterface
{
	private $userProvider;
	private $app;
	private $conf;

	public function __construct(UserProviderInterface $userProvider, Application $app)
	{
		$this->userProvider = $userProvider;
		$this->app = $app;
		$this->conf = (isset($app['jwtoken.config']) &&is_array($app['jwtoken.config']))?$app['jwtoken.config']:array();
	}

	public function authenticate(TokenInterface $token)
	{
		$aMessages = array();
		//print $this->app['security.encoder_factory']->getEncoder($oUser)->encodePassword($password,$oUser->getSalt());
		$ok = false;
		foreach($this->conf as $conf){
			$subConfig = $conf['conf'];
			switch($conf['provider']){
				case 'db':
					try{
						$oUser = $this->userProvider->loadUserByUsername($token->getUsername());
						if($subConfig['encryptedPassword']){
							$ok = ($this->app['security.encoder_factory']->getEncoder($oUser)->isPasswordValid($oUser->getPassword(), $token->getCredentials(), $oUser->getSalt()));
						}else{
							$ok = ($oUser->getPassword()==$token->getCredentials() && !empty($token->getCredentials()));
						}
					}catch(UsernameNotFoundException $e){
						$aMessages[]='The JWT authentication failed. User not found. '.$e->getMessage(). ' ('.$conf['provider'].')';
					}catch(Exception $e){
						$aMessages[]='The JWT authentication failed. '.$e->getMessage();
					}
				break;
				case 'ldap':
					$ldaprdn  = $subConfig['domain']."\\".$token->getUsername();     // DN ou RDN LDAP
					$ldapconn = ldap_connect($subConfig['server']);
					try{
						$ok = ($ldapconn && ldap_bind($ldapconn, $ldaprdn, $token->getCredentials()));
						if(!$ok){
							ldap_get_option($ldapconn, LDAP_OPT_DIAGNOSTIC_MESSAGE, $extended_error);
							$aMessages [] = $extended_error;
						}else{
							$userName = sprintf($subConfig['dbUsernameTpl'], $token->getUsername());
							$oUser = $this->userProvider->loadUserByUsername($userName);
						}
					}catch(UsernameNotFoundException $e){
						$aMessages[]='The ldap authentication failed. User not found. '.$e->getMessage(). ' ('.$conf['provider'].')';
					}catch(Exception $e){
						$aMessages[]='The ldap authentication failed. '.$e->getMessage();
					}
				break;
			}
			if($ok){
				break;
			}
		}
		if($ok){
			$authenticatedToken = new JWTokenUserToken($oUser->getRoles());
			$authenticatedToken->setUser($oUser);
			return $authenticatedToken;
		}
		//die('--'.__LINE__);
		throw new AuthenticationException('The JWT authentication failed. '.implode(',',$aMessages));
	}

	public function getUserToken( $user,array $options=array())
	{
		$options['key']='Very_secret_key';
		return JWT::encode(array(
			'name'	=> $user->getUsername(),
			'roles'	=> $user->getRoles(),
			'exp'	=> time() + 24*60*60//$this->lifeTime
		), $options['key']);
	}

	public function supports(TokenInterface $token)
	{
		return $token instanceof JWTokenUserToken;
	}
}