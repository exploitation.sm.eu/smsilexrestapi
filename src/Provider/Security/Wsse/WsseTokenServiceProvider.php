<?php

namespace  SM\SilexRestApi\Provider\Security\Wsse;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SM\SilexRestApi\Security\User\UserProvider;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class WsseTokenServiceProvider implements ServiceProviderInterface {
	protected $app;

	public function register(Application $app) {
		$this->app = $app;
		$app['security.authentication_listener.factory.wsse'] = $app->protect(function ($name, $options) use ($app) {
			$app['security.authentication_provider.' . $name . '.wsse'] = function() use ($app){
				return new WsseTokenProvider(new UserProvider($app['db']), __DIR__.'/security_cache');
			};

			$app['security.authentication_listener.'.$name.'.wsse'] = function() use ($app){
		print "ee ".__LINE__;
				return new WsseTokenListener(new TokenStorage(), $app['security.authentication_manager']);
			};

			return array(
				'security.authentication_provider.'.$name.'.wsse',
				'security.authentication_listener.'.$name.'.wsse',
				null,
				'pre_auth'
			);
		});

	}

	public function boot(Application $app) {
	}
}