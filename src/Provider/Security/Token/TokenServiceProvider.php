<?php

namespace  SM\SilexRestApi\Provider\Security\Token;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class TokenServiceProvider implements ServiceProviderInterface {
	const AUTH_VALIDATE_CREDENTIALS = 'auth.validate.credentials';
	const AUTH_VALIDATE_TOKEN = 'auth.validate.token';
	const AUTH_NEW_TOKEN = 'auth.new.token';
	const AUTH_NEEDED = 'auth.path.needed';
	protected $app;

	protected function subRegister(){
	}

	public function register(Application $app) {
		$this->app = $app;
		$this->subRegister();

		$this->app[self::AUTH_VALIDATE_CREDENTIALS] = $this->app->protect ( function ($user, $pass) {
			return $this->validateCredentials ( $user, $pass );
		} );

		$this->app[self::AUTH_VALIDATE_TOKEN] = $this->app->protect ( function ($token) {
			return $this->validateToken ( $token );
		} );

		$this->app[self::AUTH_NEW_TOKEN] = $this->app->protect ( function (UserInterface $user,$options=array()) {
			return $this->getNewTokenForUser ( $user ,$options);
		} );

		$this->app[self::AUTH_NEEDED] = $this->app->protect ( function ($path,$aAuthProtect) {
			return $this->isAuthNeededForPath ( $path, $aAuthProtect);
		} );
	}

	public function boot(Application $app) {
	}

	protected function validateCredentials($user, $pass) {
		return !empty($user) && $user == $pass;
	}

	protected function validateToken($token) {
		return $token == 'a';
	}

	protected function getNewTokenForUser(UserInterface $user,array $options=array()){
		return 'a';
	}
	protected function isAuthNeededForPath($path,$aAuthProtect){
		return in_array ( $path, $aAuthProtect);
	}
}