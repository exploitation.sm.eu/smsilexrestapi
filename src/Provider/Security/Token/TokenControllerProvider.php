<?php

namespace  SM\SilexRestApi\Provider\Security\Token;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\Request;
use Silex\ControllerProviderInterface;
use Silex\Application;

class TokenControllerProvider implements ControllerProviderInterface {
	const VALIDATE_CREDENTIALS = '/validateToken';
	const TOKEN_HEADER_KEY = 'X-Token';
	const TOKEN_REQUEST_KEY = '_token';
	protected $baseRoute;

	public function setBaseRoute($baseRoute) {
		$this->baseRoute = $baseRoute;

		return $this;
	}

	public function connect(Application $app) {
		$this->setUpMiddlewares ( $app );

		return $this->extractControllers ( $app );
	}

	protected function extractControllers(Application $app) {
		$controllers = $app['controllers_factory'];
		$controllers->get ( self::VALIDATE_CREDENTIALS, function (Request $request) use($app) {
			$user = $app[TokenServiceProvider::AUTH_VALIDATE_CREDENTIALS]( $request->get ('username'), $request->get ('password') );
			return $app->json ( [
					'status'	=> !!$user,
					'info'		=> !!$user ? [
					'token'		=> $app[TokenServiceProvider::AUTH_NEW_TOKEN] ( $user ,[]),
					'user'		=> $user->getUsername(),
					'roles'		=> implode(',',$user->getRoles()),
					] : [ ]
					] );
		} );
		return $controllers;
	}

	protected function setUpMiddlewares(Application $app) {
		$app->before ( function (Request $request) use($app) {
			if (! $this->isAuthRequiredForPath ($app, $request->getPathInfo() )) {
				$this->getJWTTokenFromRequest($request);
				if (! $this->isValidTokenForApplication ( $app, $this->getTokenFromRequest ( $request ) )) {
					throw new AccessDeniedHttpException ( 'Access Denied' );
				}
			}
		} );
	}

	protected function getTokenFromRequest(Request $request) {
		return $request->headers->get ( self::TOKEN_HEADER_KEY, $request->get ( self::TOKEN_REQUEST_KEY ) );
	}

	protected function isAuthRequiredForPath(Application $app,$path) {
		return $app[TokenServiceProvider::AUTH_NEEDED]($path,[$this->baseRoute . self::VALIDATE_CREDENTIALS]);
	}

	protected function isValidTokenForApplication(Application $app, $token){
		return $app[TokenServiceProvider::AUTH_VALIDATE_TOKEN]($token);
	}
}