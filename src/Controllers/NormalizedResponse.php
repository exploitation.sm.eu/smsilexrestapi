<?php
namespace SM\SilexRestApi\Controllers;

use SM\SilexRestApi\Component\Normalizer\EntitiesNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;

trait NormalizedResponse{

	protected function formatResponse(Request $request,Application $app,$var){
		if(is_null($var)){
			throw new HttpException(404,'Entity not found.');
		}
		$ttl = 0;
		//$ttl = 100;
		if(is_array($var) && array_key_exists('_ttl',$var)){
			$ttl = $var['_ttl'];
		}
		if(is_object($var) && property_exists('_ttl',$var)){
			$ttl = $var->_ttl;
		}
		return (new JsonResponse((new EntitiesNormalizer())->normalize($var)))
		->setPublic()
		->setTtl			($ttl)
		->setSharedMaxAge	($ttl)
		->setMaxAge			($ttl)
		->setEncodingOptions(JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_PRETTY_PRINT);
	}
}