<?php
namespace SM\SilexRestApi\Controllers;

use DDesrosiers\SilexAnnotations\Annotations as SLX;
use Monolog\Logger;
use Silex\Application as SilexApplication;
use SM\SilexRestApi\Silex\Application;
use Symfony\Component\HttpFoundation\Request;

abstract class restEntityController {
	use NormalizedResponse;
	protected $log;
	protected $entityClass=null;

	function __construct(){
		$this->log = new Logger(str_replace('\\','.',get_class($this)));
	}

	function connect(SilexApplication $app){
		//return $app['annot']->process(get_class($this), false, true);
	}

	/** @return ModelCriteria */
	function restEntity(){
		$class = $this->entityClass;
		return new $class();
	}

	/** @return ActiveRecordInterface */
	function restEntityQuery(){
		$class = $this->entityClass.'Query';
		return new $class();
	}

	/**
	 * @SLX\Route(
	 *      @SLX\Request(method="GET", uri=""),
	 * )
	 */
	function getAll(Request $request,Application $app){
		return $this->formatResponse($request,$app, $this->restEntityQuery()
			->filterByArray(json_decode($request->get('filter','{}'),true))
			->find()
			->getData()
		);
		/*
		json_decode($request->get('filter','{}'),true),
		json_decode($request->get('order','{}'),true),
		$request->get('limit',null),
		$request->get('offset',null)
		*/
	}

	/**
	 * @SLX\Route(
	 *      @SLX\Request(method="GET", uri="{id}"),
	 * )
	 */
	function get(Request $request,Application $app,$id){
		return $this->formatResponse($request,$app, $this->restEntityQuery()
			->findPk($id)
		);
	}

	/**
	 * @SLX\Route(
	 *      @SLX\Request(method="PUT", uri="{id}"),
	 * )
	 */
	function put(Request $request,Application $app,$id){
		return $this->formatResponse($request,$app, ['result'=>$this->_put($request,$id)]);
	}

	/**
	* @SLX\Route(
	*      @SLX\Request(method="POST", uri="/"),
	* )
	*/
	function post(Request $request,Application $app){
		return $this->formatResponse($request,$app, ['id'=>$this->_post($request)]);
	}

	protected function extractJsonBody(Request $request){
		$data=array();
		if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
			$data = json_decode($request->getContent(), true);
		}
		return $data;
	}

	protected function _put(Request $request,$id){
		$oEntity = $this
			->restEntityQuery()
			->findPK($id);
		$oEntity->fromArray($this->extractJsonBody($request));
		return $oEntity->save();
	}

	protected function _post(Request $request){
		//$sClassName = $this->restEntityQuery()->getTableMap()->getClassName();
		//$oEntity = new $sClassName();
		$oEntity = $this->restEntity();
		$oEntity->fromArray($this->extractJsonBody($request));
		$oEntity->save();
		return $oEntity->getPrimaryKey();
	}
}