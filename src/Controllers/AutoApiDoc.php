<?php
namespace SM\SilexRestApi\Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class AutoApiDoc{
	public function index(Request $request,Application $app){
		$app['twig']->setLoader(new \Twig_Loader_Chain(array(
			$app['twig']->getLoader(),
			new \Twig_Loader_Filesystem (array(
				__DIR__ . '/views/' 
			)) 
		)));
		return $app['twig']->render(str_replace(array(__NAMESPACE__.'\\','::'),array('','.'),__METHOD__).'.twig', array(
			'routes' => $app['api.doc']()
		));
	}
}