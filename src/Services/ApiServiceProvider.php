<?php
namespace SM\SilexRestApi\Services;
use Silex\ServiceProviderInterface;
use Silex\Application;
use Doctrine\Common\Annotations\AnnotationReader;
use ReflectionClass;
use ReflectionMethod;
use SM\SilexRestApi\Component\Utilities as U;

class ApiServiceProvider implements ServiceProviderInterface
{
	public function register(Application $app)
	{
		$app['api.doc'] = $app->protect(function () use ($app) {
			$routes=array();
			$annotationCache = array();
			foreach ($app['routes'] as $route) {
				if ($route->hasDefault('_controller')) {
					try {
						$oController = $route->getDefault('_controller');
						$sController = trim(is_string($oController)?$oController:get_class($oController));
						$path		= $route->getPath();

						if(!array_key_exists($path,$routes)){
							$routes[$path]=array('methods'=>array(),'hash'=>md5($path),'path'=>$path,'actions'=>array());
						}

						$routes[$path]['methods']= array_merge($routes[$path]['methods'],$route->getMethods());
						asort($routes[$path]['methods']);

						if($sController != 'JDesrosiers\Silex\Provider\OptionsController'){
							$aTmp = explode('::',$sController,2);
							if($sController!='Closure' && !array_key_exists($aTmp[0],$annotationCache)){
								try {
									$oClass = new ReflectionClass($aTmp[0]);
									foreach($oClass->getMethods() as $oMethod){
										/** @var $oMethod \ReflectionMethod */
										$annotationCache[strtolower($oMethod->getName())] = implode("\n",array_map(function($line){
											return trim($line);
										},explode("\n",$oMethod->getDocComment())));
									}
								}catch(\ReflectionException $e){
									$annotationCache[strtolower($aTmp[1])] = $e->getMessage();
								}
							}

							$routes[$path]['actions'][implode(',',$route->getMethods())]=array(
									'methods'	=> $route->getMethods(),
									'controller'=> $sController,
									'comment'	=> U::akead(strtolower($aTmp[1]),$annotationCache,''),
									'condition'	=> trim($route->getCondition())
							);
						}
					} catch (\InvalidArgumentException $e) {
					}
				}
			}

			uasort($routes,function($a,$b){
				$k=array('path','methods');
				if($a[$k[0]] == $b[$k[0]]) {
					if($a[$k[1]] == $b[$k[1]]) {
						return 0;
					}
					return ($a[$k[1]] > $b[$k[1]]) ? -1 : 1;
				}
				return ($a[$k[0]] > $b[$k[0]]) ? -1 : 1;
			});
			return $routes;
		});
	}

	public function boot(Application $app)
	{
	}

}