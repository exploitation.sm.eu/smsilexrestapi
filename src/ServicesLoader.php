<?php

namespace SM\SilexRestApi;

use Silex\Application;
use Silex\ServiceProviderInterface;
abstract class ServicesLoader implements ServiceProviderInterface{
	protected $app;
	
	abstract public function bindServicesIntoContainer();
	
	public function register(Application $app){
		$this->app = $app;
		$this->bindServicesIntoContainer();
	}
	
	public function boot(Application $app){
	}
}