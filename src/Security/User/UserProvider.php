<?php

namespace SM\SilexRestApi\Security\User;

use Propel\Runtime\ActiveQuery\ModelCriteria;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
/**
 *
	CREATE TABLE `users` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(100) NOT NULL DEFAULT '',
	`password` VARCHAR(255) DEFAULT NULL,
	`salt` VARCHAR(255) NOT NULL DEFAULT '',
	`roles` VARCHAR(255) NOT NULL DEFAULT '',
	`name` VARCHAR(100) NOT NULL DEFAULT '',
	`time_created` INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`username` VARCHAR(100),
	`isEnabled` TINYINT(1) NOT NULL DEFAULT 1,
	`confirmationToken` VARCHAR(100),
	`timePasswordResetRequested` INT(11) UNSIGNED,
	PRIMARY KEY (`id`),
	UNIQUE KEY `unique_email` (`email`),
	UNIQUE KEY `username` (`username`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;


	CREATE TABLE `user_custom_fields` (
	user_id INT(11) UNSIGNED NOT NULL,
	attribute VARCHAR(50) NOT NULL DEFAULT '',
	value VARCHAR(255) DEFAULT NULL,
	PRIMARY KEY (user_id, attribute)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */
class UserProvider implements UserProviderInterface
{
	private $userQuery;
	protected $userNameCol;
	protected $passwordCol;
	protected $rolesCol;

	public function __construct($userQueryClass,$userNameCol='username',$passwordCol='password',$rolesCol='roles')
	{
		$this->userQueryClass	= $userQueryClass;
		$this->userNameCol		= $userNameCol;
		$this->passwordCol		= $passwordCol;
		$this->rolesCol			= $rolesCol;
	}

	public function loadUserByUsername($username)
	{
		$userQueryClass = $this->userQueryClass;
		$aQuery=array();
		$aQuery[$this->userNameCol]=strtolower($username);
		$userQuery = new $userQueryClass();
		$user = $userQuery->findOneByArray($aQuery);
		if (!$user) {
			throw new UsernameNotFoundException( sprintf ( 'Username [%s] does not exist.', $username ) );
		}
		$user = $user->toArray();
		return new User($user[$this->userNameCol], $user [$this->passwordCol], explode ( ',', $user [$this->rolesCol] ), true, true, true, true );
	}

	public function refreshUser(UserInterface $user)
	{
		if (! $user instanceof User) {
			throw new UnsupportedUserException ( sprintf ( 'Instances of "%s" are not supported.', get_class ( $user ) ) );
		}

		return $this->loadUserByUsername ( $user->getUsername () );
	}

	public function supportsClass($class)
	{
		return $class === 'Symfony\Component\Security\Core\User\User';
	}
}