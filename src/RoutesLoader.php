<?php

namespace SM\SilexRestApi;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Config\FileLocator;
use Silex\ServiceProviderInterface;
use Silex\Application;
use SM\SilexRestApi\Component\Routing\Loader\AnnotatedYamlFileLoader;
use Doctrine\Common\Cache\ArrayCache;
use DDesrosiers\SilexAnnotations\AnnotationServiceProvider;
use Symfony\Component\HttpFoundation\Response;

abstract class RoutesLoader implements ServiceProviderInterface{
	protected $app;

	public function register(Application $app){
		$this->app = $app;
		$this->app->register(new AnnotationServiceProvider(), array(
			"annot.cache" => new ArrayCache()
		));

		$this->instantiateControllers();
		$this->app->get('/', function () {
			return new Response('<h1>Root</h1>',200);
		});
		
		$this->mountAutoDoc();
		if(isset($app['smroutesloader.path'])){
			$this->bindRoutesToControllers($app['smroutesloader.path'],'routes.yml');
		}
		$this->extendsRoutes();
	}
	
	public function boot(Application $app){
	}
	
	protected function mountAutoDoc(){
		$mountPoint = $this->app["controllers_factory"];
		$mountPoint->get('/', "SM\SilexRestApi\Controllers\AutoApiDoc::index");
		$this->app->mount('/api', $mountPoint);
	}

	public function bindRoutesToControllers($sPath,$sFile='routes.yml'){
		$this->app['routes'] = $this->app->extend('routes', function (RouteCollection $routes, Application $app) use ($sPath,$sFile) {
			$loader		= new AnnotatedYamlFileLoader(new FileLocator($sPath),$app);
			$collection	= $loader->load($sFile);
			$routes->addCollection($collection);
			return $routes;
		});
	}

	abstract protected function instantiateControllers();
	abstract protected function extendsRoutes();

}