<?php

namespace SM\SilexRestApi\Silex;

use Silex\Application as SilexApplication;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Http\SecurityEvents;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Core\User\InMemoryUserProvider;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use SM\SilexRestApi\Provider\Security\JWToken\JWTokenServiceProvider;
use SM\SilexRestApi\Provider\Security\JWToken\JWTokenControllerProvider;
use SM\SilexRestApi\Provider\Security\JWToken\JWTokenFactory;
use SM\SilexRestApi\Security\User\UserProvider;
use JDesrosiers\Silex\Provider\CorsServiceProvider;
use Monolog\Logger;

class Application extends SilexApplication{
	public function __construct(array $values = []){
		parent::__construct($values);
		$this->initCORS();
		$this->initAccept();
		$this->initErrorHandlers();
		$this->initSecurity();
		$this->register(new ServiceControllerServiceProvider());
	}

	protected function initCORS(){
		$this->register(new CorsServiceProvider($this), array(
			"cors.allowOrigin" => "*",
		));
		$this->after($this["cors"]);
	}

	protected function initAccept(){
		$this->before(function (Request $request) {
			if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
				$data = json_decode($request->getContent(), true);
				$request->request->replace(is_array($data) ? $data : array());
			}
		});
	}

	protected function initErrorHandlers(){
		$this->error(function (\Exception $e, $code) {
			$aError = array(
				'statusCode'=> $code,
				'class'		=> get_class($e),
				'message'	=> $e->getMessage(),
				'stacktrace'=> $e->getTraceAsString()
			);

			$this['logger']->error($e->getMessage());
			$this['logger']->error('',[implode("\n",array_slice(explode("\n",$e->getTraceAsString()),0,4))]);

			if($this['debug']){
				header('Content-type: text/html');
				print "<pre>";var_dump($aError);print "</pre>";
				die();
				return new Response($aError);
			}else{
				return (new JsonResponse($aError))->setEncodingOptions(JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_PRETTY_PRINT);
			}
		});
	}

	public function initSecurity(){
		$this['security.encoder.digest'] = $this->share(function ($app) {
			// use the sha1 algorithm
			// don't base64 encode the password
			// use only 1 iteration
			return new MessageDigestPasswordEncoder('sha1', false, 1);
		});

		$debugEventCallback = function(Event $event){
			print $event->getName();
		};
		$this->on(AuthenticationEvents::AUTHENTICATION_FAILURE	, $debugEventCallback);
		$this->on(AuthenticationEvents::AUTHENTICATION_SUCCESS	, $debugEventCallback);
		$this->on(SecurityEvents::INTERACTIVE_LOGIN				, $debugEventCallback);
		$this->on(SecurityEvents::SWITCH_USER					, $debugEventCallback);

		$this->register(new JWTokenServiceProvider());
		$this->register(new SecurityServiceProvider());
		$this->mount('/auth',(new JWTokenControllerProvider())->setBaseRoute('/auth') );
	}

	protected function initAuth(){
		/*
		//$app['security.access_rules'] = array(
		//	array('^/api', 'ROLE_ADMIN', 'https'),
		//	array('^.*$', 'ROLE_USER'),
		//);

		$this['security.firewalls']=[
			'login' => [
				'pattern' => '^/login|/register|/oauth',
				'anonymous' => true,
			],
			'api' => [
				'pattern' => '^/api(.*)',
				'form' => [
					'login_path' => '/login',
					'check_path' => '/admin/login_check'
				],
				'logout' => [
					'logout_path'		 => '/admin/logout',
					'invalidate_session' => false
				],
				'users' => $this['users'] $this->share(function () {
					return new UserProvider($this['db']);
				}),
				'jwt'=> array(
					'use_forward'				=> true,
					'stateless'					=> true,
					'require_previous_session'	=> false,
				)
			]
		];
		$this['security.jwt'] = array(
			'secret_key'	=> 'hdjksdsqqgdqs(è§(ghjv213dkjqs21dq`*-$^ù6782)qds!dçàq31sdsqbd,n&éedr12(sde(squkd,hfrqcrfq',
			'life_time'		=> 86400,
			'options'		=> array(
				'username_claim'=> 'sub', // default name, option specifying claim containing username
				'header_name'	=> 'X-Access-Token', // default null, option for usage normal oauth2 header
				'token_prefix'	=> 'Bearer',
			)
		);
		$app->post('/api/login', function(Request $request) use ($app){
			$vars = json_decode($request->getContent(), true);

			try {
				if (empty($vars['_username']) || empty($vars['_password'])) {
					throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $vars['_username']));
				}

				 * @var $user User
				$user = $app['users']->loadUserByUsername($vars['_username']);

				if (! $app['security.encoder.digest']->isPasswordValid($user->getPassword(), $vars['_password'], '')) {
					throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $vars['_username']));
				} else {
					$response = [
					'success' => true,
					'token' => $app['security.jwt.encoder']->encode(['name' => $user->getUsername()]),
					];
				}
			} catch (UsernameNotFoundException $e) {
				$response = [
				'success' => false,
				'error' => 'Invalid credentials',
				];
			}

			return $app->json($response, ($response['success'] == true ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST));
		});
		$this->register(new SecurityServiceProvider(),array(
			'security.providers' => array(
				'api_key_user_provider'=>array(
					'id'	=>'api_key_user_provider'
				)
			),
			'security.access_rules' => array(
				array('^/api', 'ROLE_USER'),
			),
			'security.firewalls' => array(
				'login' => array(
					'http'		=> true,
					'pattern'	=> '^(/login)|(/register)|(/oauth)|(/auth)',
					'anonymous'	=> true,
				),
				'home' => array(
					'pattern'	=> '^/$',
					'security'	=> false,
				),
				'api' => array(
					'pattern'	=> '^/api',
					'users'		=> $this->share(function (){
							return new UserProvider($this['db']);
					}),
					'security'	=> true,
					//'http'		=> false,
					//'simple_preauth'=> array(
					//	'authenticator'		=> 'apikey_authenticator'
					//),
					//'provider'			=> 'api_key_user_provider',
					//'jwt'		=> array(
					//	'use_forward'				=> true,
					//	'stateless'					=> true,
					//	'require_previous_session'	=> false,
					//)
					)
				)
		));*/
	}
}